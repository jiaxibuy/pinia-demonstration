import { h } from 'vue'
import { NIcon } from 'naive-ui'

// icon 渲染
 // eslint-disable-next-line
export function renderIcon(icon) {
  return () => h(NIcon, null, { default: () => h(icon) })
}